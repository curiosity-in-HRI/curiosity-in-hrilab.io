#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = ''
SITENAME = 'Workshop on Curiosity in HRI'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/London'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

# PLUGINS
PLUGIN_PATHS = [ '../pelican-plugins']
PLUGINS = [ 'i18n_subsites' ]
JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}

# THEME settings
THEME = '../pelican-themes/pelican-bootstrap3'

STATIC_PATHS = [
    'theme/css/bootstrap.curiosity.min.css',
    ]
BOOTSTRAP_THEME ='curiosity'
PADDED_SINGLE_COLUMN_STYLE = True

DISPLAY_PAGES_ON_MENU = True
PAGES_SORT_ATTRIBUTE = 'menuorder'
HIDE_SITENAME = False

#
PAGE_URL = '{slug}.html'
PAGE_SAVE_AS = '{slug}.html'

LOAD_CONTENT_CACHE = False

# 
PLUGINS.append('bootstrapify')

BOOTSTRAPIFY = {
    'table': ['table', 'table-striped', 'table-hover'],
    'img': ['img-fluid', 'border', 'border-primary'],
    'blockquote': ['blockquote'],
    #'code': ['badge badge-light']
}