Call for papers
###############

:menuorder: 2

Topics of interest
---------------------------------------------

- Human perceptions of curious robots
- Long-term HRI studies with curious robots
- Tools, benchmarks for curiosity-driven learning
- Challenges in curiosity-driven learning in HRI
- Real-world applications of curious robots
- Metrics for evaluating curiosity-driven learning in HRI
- Curiosity-driven learning
- Self-supervised learning
- Intrinsic motivations
- Active learning
- Lifelong learning
- Lifelong (Long-Term) human-agent or multi-agent interactions
- Ethical and legal considerations for curiosity-driven learning in HRI