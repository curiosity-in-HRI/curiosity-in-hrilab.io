Speakers
########

:menuorder: 4

- Professor Edith Lawis
- `Professor Maya Cakmak <https://homes.cs.washington.edu/~mcakmak/>`_
- `Dr. Georg Martius <https://www.is.mpg.de/person/gmartius>`_
- `Dr. Christoph Salge <https://researchprofiles.herts.ac.uk/portal/en/persons/christoph-salge%28470e3494-2bf6-458f-9e7a-169ac762c2ff%29/publications.html>`_
- `Dr. Goren Gordon <https://gorengordon.com/>`_