Workshop on Curiosity in HRI
############################

:url: 
:save_as: index.html
:menuorder: 1

The goal of this workshop is to provide a medium for researchers to present their research in progress and discuss topics related to curiosity-driven learning in robotics and HRI.

This workshop is cosidered for submission as a full day event in conjunction with the `ACM/IEEE International Conference on Human-Robot Interaction (HRI) <https://humanrobotinteraction.org/2022/>`_ 2022, held on March 7-10 in Sapporo, Hokkaido, Japan

Importance of curiosity-driven learning
-------------------------------------------------------------------------------------------------

One of the fundamental modes of learning in children is through curiosity. Children (and adults) interact with new people, learn novel objects, activities and other stimuli through curiosity and other motivations. Creating autonomous robots that learn continually through intrinsic curiosity may result in breakthroughs in artificial intelligence. Such robots could continue to learn about themselves and the world around them through curiosity, thus improving their abilities over their lifetime. Although recent works on curiosity in different fields have produced significant results, most of these works have focused on constrained simulated environments which do not involve human interaction. However, in the real-world applications such as healthcare, home-assistance etc., robots generally have to interact with humans on a regular basis. In these scenarios, it is imperative that curiosity is directed towards asking important information from the humans when needed rather than simply learning in an unsupervised manner. Further, there is limited work on how humans perceive such curious robots and if humans prefer curious robots that adapt over time than other robots that simply perform their assigned tasks. In this workshop, our goal is to bring together researchers and practitioners in various AI fields to discuss about the role of robot curiosity in real-world applications and its implications in human-robot interaction (HRI). 

Topics of interest
---------------------------------------------

- Human perceptions of curious robots
- Long-term HRI studies with curious robots
- Tools, benchmarks for curiosity-driven learning
- Challenges in curiosity-driven learning in HRI
- Real-world applications of curious robots
- Metrics for evaluating curiosity-driven learning in HRI
- Curiosity-driven learning
- Self-supervised learning
- Intrinsic motivations
- Active learning
- Lifelong learning
- Lifelong (Long-Term) human-agent or multi-agent interactions
- Ethical and legal considerations for curiosity-driven learning in HRI