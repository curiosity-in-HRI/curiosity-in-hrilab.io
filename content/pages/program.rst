Tentitive Program
#################

:menulabel: Program
:slug: program
:menuorder: 3
:status: hidden

We plan to run a full-day event in which we expect 10-15 papers presented as pre-recorded talks, 4 invited talks and a panel discussion. The invited talks will be given by 5 invited speakers in areas relevant to the theme of the workshop. The panel discussion will be among 6 panelists from various fields related to curiosity in HRI. The tentative program of the workshop is as follows:

- 9:00 AM - 9:10 AM & Opening
- 9:10 AM - 9: 50 AM & Invited Talk 1: TBD
- 9:50 AM - 10:30 AM & Invited Talk 2: TBD
- 10:30 AM - 10:45 AM & Coffee Break
- 10:45 AM - 11:45 AM & Paper Presentations
- 11:45 AM - 12:25 PM & Invited Talk 3: TBD
- 12:25 PM - 01:05 PM & Invited Talk 4: TBD
- 01:05 PM - 02:00 PM & Lunch Break
- 02:00 PM - 03:00 PM & Paper Presentations
- 03:00 PM - 03:40 PM & Invited Talk 5: TBD
- 03:40 PM - 04:00 PM & Coffee Break
- 04:00 PM - 05:30 PM & Panel Discussion
- 05:30 PM - 05:40 PM & Closing Remarks